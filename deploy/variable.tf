variable "prefix" {
  type        = string
  default     = "raad"
  description = "Prefix for Recipe App Api Devops"
}

variable "project" {
  type        = string
  default     = "recipe-app-api-devops"
  description = "Name of our Project"
}

variable "contact" {
  type        = string
  default     = "kingaws101@gmail.com"
  description = "Contact info for Project Manager"
}

